# README #

This is the repository of the Variable Costs and Size Bin Packing Problem with Time-Dependent Costs instances

### Naming ###
The instances are in the OPL Studio DAT file


* Naming MPVSBPP_SET1_ITX_ITVY_NTZ_TSA_WT1_VT1_REPK.dat where
	* X: numbero of items
	* Y: IT Volumes type
	* Z: numer of vehicle types
	* A: numer of timeslots
	* K: number of replica
	
* Version: 1.0 Last Uodate 27/05/2019

### File Format ###

outFile: File where to put the output

* m: Number of orders
* o: Total number of vehicles (as sum of the available vehicles over all the types)
* p: Number of time intervals
* num: Number of vehicle types
* demand: Quantity of each order
* Depot_Cap: Depot capacity in each time interval
* Tarif:  Cost for renting space in each time interval
* VeicVolume: Avalilable volume (one for each vehicle type)
* VeicCost: Cost for vehicle renting (one for each vehicle type)
* VeicType: Type of vehicle associated to each individual vehicle
* Elect_vehicle: For each individual vehicle and each time interval, it gives the cost for the delivery of one order (independently on its demand)


